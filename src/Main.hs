module Main where

import           Booleron
import           Booleron.Parser
import           Options.Applicative

passesOpt, decentOpt, sizeOpt, resultOpt :: Parser Int
passesOpt = option auto
  $ long "passes"
  <> short 'p'
  <> help "number of passes to make on the expression before considering it resolved"

decentOpt = option auto
  $ long "decent"
  <> short 'd'
  <> help "how deep to recurse into the expression on each pass"

sizeOpt = option auto
  $ long "size"
  <> short 's'
  <> help "how many results to hang onto between passes"

resultOpt = option auto
  $ long "results"
  <> short 'r'
  <> help "how many results are desired (this algorithm may return more than one answer)"

configOpts :: Parser Config
configOpts = Config
  <$> passesOpt ->> passes
  <*> decentOpt ->> decent
  <*> sizeOpt   ->> size
  <*> resultOpt ->> result
  where
    x ->> y = x <|> pure (y defaultConfig)

exprArg :: Parser (Maybe String)
exprArg = optional $ argument str $ metavar "EXPRESSION"

main :: IO ()
main = do
  (expr, config) <- execParser opts
  print . simplify config =<< parseAST <$> fallbackToStdin expr
  where
    opts = info (helper <*> ((,) <$> exprArg <*> configOpts))
         $ fullDesc
         <> progDesc "Simplify a boolean EXPRESSION"
         <> header "Booleron - a brute force boolean expression simplifier"

    fallbackToStdin (Just arg) = return arg
    fallbackToStdin _ = getContents
