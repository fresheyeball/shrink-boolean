{-# LANGUAGE FlexibleInstances #-}
module Booleron
  ( module Booleron
  , module Booleron.AST
  , module Booleron.Identities
) where

import           Booleron.AST
import           Booleron.Identities
import           Data.List           (foldr, sort)
import           Data.Map            (Map, lookup)
import           Data.Maybe          (catMaybes)
import qualified Data.Set            as S
import           Data.Text           hiding (foldr, replicate, take)
import           Prelude             hiding (foldr)


evaluate :: Map Text Bool -> AST -> Maybe Bool
evaluate hash ast = case ast of
  P x -> eval x
  V v -> Data.Map.lookup v hash
  Tautology -> Just True
  Contradiction -> Just False
  Not x -> not <$> eval x
  x := y -> op (==) x y
  x :!= y -> op (/=) x y
  x :| y -> op (||) x y
  x :& y -> op (&&) x y
  where
    eval = evaluate hash
    op o x y = o <$> eval x <*> eval y

applyMatches :: AST -> [AST]
applyMatches x = x : catMaybes (($ x) <$> identities)

data Config = Config
  { passes :: Int
  , decent :: Int
  , size   :: Int
  , result :: Int }
  deriving (Show)

defaultConfig :: Config
defaultConfig = Config 3 3 3 1

simplify :: Config -> AST -> [AST]
simplify config
  = take (result config) . sort
  . uniq . (deparen <$>)
  . sweep config

sweep :: Config -> AST -> [AST]
sweep config = sweep' 0
  where

    sweep' current | current >= decent config = applyMatches
    sweep' current =
      let

        s :: AST -> [AST]
        s = sweep' (current + 1)

        f ast' = case ast' of
          (V v)           -> pure (V v)
          P (V v)         -> pure (V v)
          P Tautology     -> pure Tautology
          P Contradiction -> pure Contradiction
          P x             -> P <$> s x
          x := y          -> liftOp (:=) x y
          x :!= y         -> liftOp (:!=) x y
          x :| y          -> liftOp (:|) x y
          x :& y          -> liftOp (:&) x y
          Not x           -> Not <$> s x
          x               -> s x

          where
            liftOp op x y = op <$> s x <*> s y

        clean :: [AST] -> [AST]
        clean = take (size config) . sort . uniq

        pass = (clean . applyMatches =<<) . (clean . f =<<)

      in foldr (.) id (replicate (passes config) pass) . clean . applyMatches


uniq :: [AST] -> [AST]
uniq = S.toList . S.fromList

deparen :: AST -> AST
deparen ast' = case ast' of
  P (V v)         -> V v
  P Tautology     -> Tautology
  P Contradiction -> Contradiction
  P x             -> deparen x
  x := y          -> liftOp (:=) x y
  x :!= y         -> liftOp (:!=) x y
  x :| y          -> liftOp (:|) x y
  x :& y          -> liftOp (:&) x y
  Not x           -> Not (deparen x)
  x               -> x

  where
    liftOp op x y = deparen x `op` deparen y
