{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Booleron
import           Data.Map
import           Data.Maybe            (fromMaybe)
import           Data.Text             hiding (head, zip, index)
import           Prelude
import           Test.Tasty
import           Test.Tasty.QuickCheck as QC

pureText :: Char -> Text
pureText = Data.Text.singleton

instance Arbitrary AST where
  arbitrary = limited (10 :: Int)

    where
    v = V . pureText <$> elements ['a'..'z']

    limited n | n <= 0 = v
    limited n = let l = limited (n - 1) in
      frequency
        [ (8, P <$> l)
        , (1, pure Tautology)
        , (1, pure Contradiction)
        , (8, v)
        , (2, Not <$> l)
        , (2, (:=)  <$> l <*> l)
        , (2, (:!=) <$> l <*> l)
        , (4, (:|)  <$> l <*> l)
        , (4, (:&)  <$> l <*> l)
        ]

instance {-# OVERLAPPING #-} Arbitrary [(Text, Bool)] where
  arbitrary = let
    chars = pureText <$> ['a' .. 'z']
    in do
      bools <- sequence $ const arbitrary <$> chars
      return $ zip chars bools

checkId :: BoolId -> AST -> [(Text, Bool)] -> Bool
checkId identity ast xs =
  let bools = fromList xs
  in fromMaybe True $ do
    match <- identity ast
    expected <- evaluate bools ast
    actual <- evaluate bools match
    return $ expected == actual

idChecks :: TestTree
idChecks = testGroup "Identities"
         $ do (index, identity) <- zip ([0..] :: [Int]) identities
              return $ QC.testProperty ("identity -> " ++ show index)
                     $ checkId identity

specialCase :: TestTree
specialCase = testGroup "Special Cases"
            $ do (index, identity) <- zip ([0..] :: [Int]) identities
                 return $ QC.testProperty ("(a & b) | (c != c) with " ++ show index)
                        $ checkId identity
                        $ P (V "a" :& V "b") :| P (V "c" :!= V "c")

simplifyCheck :: TestTree
simplifyCheck = testGroup "Simplify"
  [ QC.testProperty "returns an equivelant statment"
    $ \ast xs -> fromMaybe True $ do
      let bools = fromList xs
      expected <- evaluate bools ast
      actual <- evaluate bools . head
             $ simplify defaultConfig ast
      return $ expected == actual
  , QC.testProperty "returns a sorter version"
    $ \ast ->
      head (simplify defaultConfig ast) <= ast
  ]

main :: IO ()
main = defaultMain
     $ testGroup "Test"
     [ idChecks
    --  , specialCase
     , simplifyCheck ]
