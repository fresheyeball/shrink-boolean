{-# LANGUAGE FlexibleInstances #-}
module Booleron.AST where

import           Data.Function (on)
import           Data.Monoid   ((<>))
import           Data.Text     hiding (length)


data AST
  = P AST -- parenthesis
  | Tautology -- tautology
  | Contradiction -- contradiction
  | V Text -- variable
  | Not AST -- negation
  | AST := AST -- comparision
  | AST :!= AST -- negated comparision
  | AST :| AST -- disjunction
  | AST :& AST -- conjunction
  deriving (Eq, Read)

infix 4 :=
infix 4 :!=
infixr 2 :|
infixr 3 :&

instance Ord AST where
  compare = compare `on` (length . show)

instance Show AST where
  show (P a') = "(" <> show a' <> ")"
  show Tautology = "True"
  show Contradiction = "False"
  show (V s) = unpack s
  show (Not a') = "not " <> show a'
  show (x := y) = show x <> " == " <> show y
  show (x :!= y) = show x <> " /= " <> show y
  show (x :| y) = show x <> " || " <> show y
  show (x :& y) = show x <> " && " <> show y

instance {-# OVERLAPPING #-} Show [AST] where
  show (x:xs) = show x <> "\n" <> show xs
  show _ = ""
