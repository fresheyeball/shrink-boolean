module Booleron.Identities where

import           Booleron.AST

identities :: [AST -> Maybe AST]
identities =

  -- Definition
  [ negation
  , conjunction
  , disjunction
  , notequals
  , equals

  -- Monotone
  , associativityDis
  , associativityCon
  , associativityNot
  , associativityEqu

  , commutativityDis
  , commutativityCon
  , commutativityNot
  , commutativityEqu

  , distributivityCoD
  , distributivityDoC
  , distributivityCoX
  , distributivityDoE

  , identityDis
  , identityCon
  , identityNot
  , identityEqu

  , annihilatorDis
  , annihilatorCon
  , annihilatorNot
  , annihilatorEqu

  , idempotenceDis
  , idempotenceCon
  , absorption1
  , absorption2

  -- Nonmonotone
  , complementation1
  , complementation2
  , doubleNegation
  , deMorgan1
  , deMorgan2

  -- Misc
  , consensus1
  , consensus2
  -- execution is SUBSTANTIALLY FASTER without these for some reason
  -- , nameless1
  -- , nameless2
  -- , nameless3
  -- , nameless4
  ]

type BoolId = AST -> Maybe AST

{-|
  Defintions
-}

negation :: BoolId
negation (Not Tautology) = Just Contradiction
negation (Not Contradiction) = Just Tautology
negation _ = Nothing

conjunction :: BoolId
conjunction (Contradiction :& Contradiction) = Just Contradiction
conjunction (Tautology :& Tautology) = Just Tautology
conjunction _ = Nothing

disjunction :: BoolId
disjunction (Contradiction :| Contradiction) = Just Contradiction
disjunction (Tautology :| Tautology) = Just Tautology
disjunction _ = Nothing

notequals :: BoolId
notequals (Contradiction :!= Contradiction ) = Just Contradiction
notequals (Tautology :!= Tautology) = Just Contradiction
notequals _ = Nothing

equals :: BoolId
equals (Contradiction := Contradiction ) = Just Tautology
equals (Tautology := Tautology) = Just Tautology
equals _ = Nothing

{-|
  Monotone Laws
-}

-- Associativity of Disjunction
associativityDis :: BoolId
associativityDis (x :| P (y :| z)) = Just $ P (x :| y) :| z
associativityDis (P (x :| y) :| z) = Just $ x :| P (y :| z)
associativityDis _ = Nothing

-- Associativity of Conjunction
associativityCon :: BoolId
associativityCon (x :& P (y :& z)) = Just $ P (x :& y) :& z
associativityCon (P (x :& y) :& z) = Just $ x :& P (y :& z)
associativityCon _ = Nothing

-- Associativity of Not Equals (Xor)
associativityNot :: BoolId
associativityNot (x :!= P (y :!= z)) = Just $ P (x :!= y) :!= z
associativityNot (P (x :!= y) :!= z) = Just $ x :!= P (y :!= z)
associativityNot _ = Nothing

-- Associativity of Equals
associativityEqu :: BoolId
associativityEqu (x := P (y := z)) = Just $ P (x := y) := z
associativityEqu (P (x := y) := z) = Just $ x := P (y := z)
associativityEqu _ = Nothing

-- Commutativity of Disjunction
commutativityDis :: BoolId
commutativityDis (x :| y) = Just $ y :| x
commutativityDis _ = Nothing

-- Commutativity of Conjunction
commutativityCon :: BoolId
commutativityCon (x :& y) = Just $ y :& x
commutativityCon _ = Nothing

-- Commutativity of Not Equals (Xor)
commutativityNot :: BoolId
commutativityNot (x :!= y) = Just $ y :!= x
commutativityNot _ = Nothing

-- Commutativity of Equals
commutativityEqu :: BoolId
commutativityEqu (x := y) = Just $ y := x
commutativityEqu _ = Nothing

-- C <-> D

-- Distributivity of Conjunction over Disjunction
distributivityCoD :: BoolId
distributivityCoD (x :& P (y :| z)) = Just $ P (x :& y) :| P (x :& z)
distributivityCoD (P (x :& y) :| P (x' :& z)) | x == x' = Just $ x :& P (y :| z)
distributivityCoD _ = Nothing

-- Distributivity of Disjunction over Conjunction
distributivityDoC :: BoolId
distributivityDoC (x :| P (y :& z)) = Just $ P (x :| y) :& P (x :| z)
distributivityDoC (P (x :| y) :& P (x' :| z)) | x == x' = Just $ x :| P (y :& z)
distributivityDoC _ = Nothing

-- Distrbutivity of Conjunction over Xor
distributivityCoX :: BoolId
distributivityCoX (x :& P (y :!= z)) = Just $ P (x :& y) :!= P (x :& z)
distributivityCoX (P (x :& y) :!= P (x' :& z)) | x == x' = Just $ x :& P (y :!= z)
distributivityCoX _ = Nothing

-- Distrbutivity of Disjunction over Equals
distributivityDoE :: BoolId
distributivityDoE (x :| P (y := z)) = Just $ P (x :| y) := P (x :| z)
distributivityDoE (P (x :| y) := P (x' :| z)) | x == x' = Just $ x :| P (y := z)
distributivityDoE _ = Nothing

-- Identity for Disjunction
identityDis :: BoolId
identityDis (x :| Contradiction) = Just x
identityDis (Contradiction :| x) = Just x
identityDis _ = Nothing

-- Identity for Conjunction
identityCon :: BoolId
identityCon (x :& Tautology) = Just x
identityCon (Tautology :& x) = Just x
identityCon _ = Nothing

-- Identity for Not Equals (Exclusive Or)
identityNot :: BoolId
identityNot (x :!= Contradiction) = Just x
identityNot (Contradiction :!= x) = Just x
identityNot _ = Nothing

-- Identity for Equals
identityEqu :: BoolId
identityEqu (x := Tautology) = Just x
identityEqu (Tautology := x) = Just x
identityEqu _ = Nothing

-- Annihilator for Disjunction
annihilatorDis :: BoolId
annihilatorDis (_ :| Tautology) = Just Tautology
annihilatorDis (Tautology :| _) = Just Tautology
annihilatorDis _ = Nothing

-- Annihilator for Conjunction
annihilatorCon :: BoolId
annihilatorCon (_ :& Contradiction) = Just Contradiction
annihilatorCon (Contradiction :& _) = Just Contradiction
annihilatorCon _ = Nothing

-- Annihilator for Not Equals (Xor)
annihilatorNot :: BoolId
annihilatorNot (x :!= x') | x == x' = Just Contradiction
annihilatorNot (x :!= Not x') | x == x' = Just Tautology
annihilatorNot (Not x :!= x') | x == x' = Just Tautology
annihilatorNot _ = Nothing

-- Annihilator for Equals
annihilatorEqu :: BoolId
annihilatorEqu (x := x') | x == x' = Just Tautology
annihilatorEqu (x := Not x') | x == x' = Just Contradiction
annihilatorEqu (Not x := x') | x == x' = Just Contradiction
annihilatorEqu _ = Nothing

-- Idempotence for Disjunction
idempotenceDis :: BoolId
idempotenceDis (x :| x') | x == x' = Just x'
idempotenceDis _ = Nothing

-- idempotence for Conjunction
idempotenceCon :: BoolId
idempotenceCon (x :& x') | x == x' = Just x'
idempotenceCon _ = Nothing

-- Absorption 1
absorption1 :: BoolId
absorption1 (x :& P (x' :| _)) | x == x' = Just x
absorption1 _ = Nothing

-- Absorption 2
absorption2 :: BoolId
absorption2 (x :| P (x' :& _)) | x == x' = Just x
absorption2 _ = Nothing


{-|
  Nonmonotone Laws
-}

-- Complementation 1
complementation1 :: BoolId
complementation1 (x :& Not x') | x == x' = Just Contradiction
complementation1 _ = Nothing

-- Complementation 2
complementation2 :: BoolId
complementation2 (x :| Not x') | x == x' = Just Tautology
complementation2 _ = Nothing

-- Double negation
doubleNegation :: BoolId
doubleNegation (Not (Not x)) = Just x
doubleNegation _ = Nothing

-- De Morgan 1
deMorgan1 :: BoolId
deMorgan1 (Not x :& Not y)   = Just $ Not (P (x :| y))
deMorgan1 (Not (P (x :| y))) = Just $ Not x :& Not y
deMorgan1 _ = Nothing

-- De Morgan 2
deMorgan2 :: BoolId
deMorgan2 (Not x :| Not y)   = Just $ Not (P (x :& y))
deMorgan2 (Not (P (x :& y))) = Just $ Not x :| Not y
deMorgan2 _ = Nothing

{-|
  Misc
-}

-- x * y + ~x * z + y * z = x * y + ~x * z
consensus1 :: BoolId
consensus1 (x :& y :| Not x' :& z :| y' :& z')
        | x == x' && y == y' && z == z' = Just $ x :& y :| Not x :& z
consensus1 (x :& y :| Not x' :& z)
        | x == x' = Just $ x :& y :| Not x' :& z :| y :& z
consensus1 _ = Nothing

-- (x + y) * (~x + z) * (y + z) = (x + y) * (~x + z)
consensus2 :: BoolId
consensus2 (P (x :| y) :& P (Not x' :| z) :& P (y' :| z'))
        | x == x' && y == y' && z == z' = Just $ P (x :| y) :& P (Not x :| z)
consensus2 (P (x :| y) :& P (Not x' :| z))
        | x == x' = Just $ P (x :| y) :& P (Not x :| z) :& P (y :| z)
consensus2 _ = Nothing

-- x + ~x * y = x + y
nameless1 :: BoolId
nameless1 (x :| Not x' :& y) | x == x' = Just (x :| y)
nameless1 (x :| y) = Just $ x :| Not x :& y
nameless1 _ = Nothing

-- x * (~x + y) = x * y
nameless2 :: BoolId
nameless2 (x :& P (Not x' :| y)) | x == x' = Just (x :& y)
nameless2 (x :& y) = Just $ x :& P (Not x :| y)
nameless2 _ = Nothing

-- x * y + x * y = x
nameless3 :: BoolId
nameless3 (x :& y :| x' :& y') | x == x' && y == y' = Just x
nameless3 _ = Nothing

-- (x + y) * (x + y) = x
nameless4 :: BoolId
nameless4 (P (x :| y) :& P (x' :| y')) | x == x' && y == y' = Just x
nameless4 _ = Nothing
