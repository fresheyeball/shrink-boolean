{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module Booleron.Parser where

import           Booleron.AST
import           Data.Text
import           Text.Parsec
import           Text.Parsec.Expr
import           Text.Parsec.Language (haskellStyle)
import           Text.Parsec.String
import           Text.Parsec.Token

lexer :: TokenParser ()
lexer = makeTokenParser $ haskellStyle
  { identStart = valid
  , identLetter = valid <|> char ' '}
  where valid = letter <|> char '_' <|> char '$'

parens' :: Parser AST -> Parser AST
parens' = fmap P . parens lexer

var :: Parser AST
var = V . strip . pack <$> identifier lexer

contents :: Parser AST -> Parser AST
contents p = do
  whiteSpace lexer
  r <- p
  eof
  return r

bool :: Parser AST
bool =  string "True"  *> return Tautology
    <|> string "T"  *> return Tautology
    <|> string "False" *> return Contradiction
    <|> string "F" *> return Contradiction

not' :: Parser AST -> Parser AST
not' p = do
  _ <- string "not " <|> string "~"
  r <- p
  return $ Not r


aexp :: Parser AST
aexp =  parens' term
    <|> not' term
    <|> bool
    <|> var


term :: Parser AST
term = buildExpressionParser table aexp
 where infixOp x f = Infix (reservedOp lexer x >> return f)
       table = [ [ infixOp "&&"  (:&)  AssocRight ],
                 [ infixOp "&"   (:&)  AssocRight ],
                 [ infixOp "||"  (:|)  AssocRight ],
                 [ infixOp "|"   (:|)  AssocRight ],
                 [ infixOp "=="  (:=)  AssocNone  ],
                 [ infixOp "="   (:=)  AssocNone  ],
                 [ infixOp "!="  (:!=) AssocNone  ],
                 [ infixOp "/="  (:!=) AssocNone  ],
                 [ infixOp "!="  (:!=) AssocNone  ] ]

parseAST :: String -> AST
parseAST input =
  case parse (contents term) "<args>" input of
    Left err -> error (show err)
    Right ast -> ast
