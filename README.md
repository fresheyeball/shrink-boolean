# Booleron

Brute force boolean simplifier.

It works by applying a list of boolean identities recursively to a boolean expression, and then sorting the results.
This works fairly well and will actually allow expressions to grow and then shrink. For example

`(x & y) | ~x` simplifies to `~x | y` without issue, even thoug it requires evaluation of sub expressions and growth

```
(x & y) | ~x        // input
     ~x | (x & y)   // commutativity applied
(~x | x) & (~x | y) // distrubtivity applied (expression grew)
       T & (~x | y) // complementation applied to sub expression
            ~x | y  // output
```

The algorithm will never resolve without limits. So tuning might be needed in some cases.
However the very shallow dive the `defaultConfig` is more than enough for most real world code.

## Basic cli usage

```
$ booleron "(foo && bar) /= (foo && baz)"
foo & baz /= bar
```

The binary also accepts stdin streams. And has options

```
Usage: booleron [EXPRESSION] [-p|--passes ARG] [-d|--decent ARG] [-s|--size ARG]
                [-r|--results ARG]
  Simplify a boolean EXPRESSION

Available options:
  -h,--help                Show this help text
  -p,--passes ARG          number of passes to make on the expression before
                           considering it resolved
  -d,--decent ARG          how deep to recurse into the expression on each pass
  -s,--size ARG            how many results to hang onto between passes
  -r,--results ARG         how many results are desired (this algorithm may
                           return more than one answer)
```

## Identities addressed

This simplifier could be written using only 3 identities from NAND logic, but it would not perform well.
As a result it documents many more identities in the list. Including:

- Defintions for
  - Negation
  - Conjunction
  - Disjunction
  - Not Equals (xor)
  - Equals (xand)
- The Monotone laws
  - Associativity
  - Commutativity
  - Distributivity
     - Conjunction over Disjunction
     - Disjunction over Conjunction
     - Conjunction over Not Equals (xor)
     - Disjunction over Equals (iff)
  - Identities (in the Monoid Sense)
  - Annihilation
  - Idempotence
     - Conjunction
     - Disjunction
  - Absorption
- Non Monotone Laws
  - Complementation
  - Double Negation
  - De Morgan
  - Consensus
- Some Misc Identities

This is not exhautive, but it's fairly complete for this purpose.
